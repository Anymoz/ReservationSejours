package models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Sejour {
    private LocalDate dateDebut;
    private LocalDate dateFin;
    private String lieu;
    private double prix;
    private String titre;
    private int nombrePersonnes;
    private Hote hote;
    private List<DemandeReservation> reservations;

    public Sejour(LocalDate dateDebut, LocalDate dateFin, String lieu, double prix, String titre, int nombrePersonnes, Hote hote) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.lieu = lieu;
        this.prix = prix;
        this.titre = titre;
        this.nombrePersonnes = nombrePersonnes;
        this.hote = hote;
        this.reservations = new ArrayList<>();
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNombrePersonnes() {
        return nombrePersonnes;
    }

    public void setNombrePersonnes(int nombrePersonnes) {
        this.nombrePersonnes = nombrePersonnes;
    }

    public Hote getHote() {
        return hote;
    }

    public void setHote(Hote hote) {
        this.hote = hote;
    }
    
    public List<DemandeReservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<DemandeReservation> reservations) {
        this.reservations = reservations;
    }
    
    public void addReservation(DemandeReservation reservation) {
        reservations.add(reservation);
    }
    
    
    public void removeReservation(DemandeReservation reservation) {
        reservations.remove(reservation);
    }

}
