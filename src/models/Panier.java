package models;

import java.util.List;

public class Panier {
    private Voyageur voyageur;
    private List<Sejour> sejours;

    public Panier(Voyageur voyageur, List<Sejour> sejours) {
        this.voyageur = voyageur;
        this.sejours = sejours;
    }

    public Voyageur getVoyageur() {
        return voyageur;
    }

    public void setVoyageur(Voyageur voyageur) {
        this.voyageur = voyageur;
    }

    public List<Sejour> getSejours() {
        return sejours;
    }

    public void setSejours(List<Sejour> sejours) {
        this.sejours = sejours;
    }

    public void ajouterSejour(Sejour sejour) {
        sejours.add(sejour);
    }

    public void supprimerSejour(Sejour sejour) {
        sejours.remove(sejour);
    }

    public void validerPanier() {
        for (Sejour sejour : this.sejours) {
            DemandeReservation demande = new DemandeReservation(sejour.getDateDebut(), sejour.getDateFin(), sejour, voyageur, sejour.getNombrePersonnes());
            voyageur.ajouterReservation(demande);
            sejour.addReservation(demande);
            sejour.getHote().setDemandesEnCours(demande);
        }
        
        sejours.clear();
    }
}
