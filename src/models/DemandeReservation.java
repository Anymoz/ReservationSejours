package models;

import java.time.LocalDate;

public class DemandeReservation extends Reservation {
    private boolean validee;
	private boolean annulee;
	
    public DemandeReservation(LocalDate dateDebut, LocalDate dateFin, Sejour sejour, Voyageur voyageur,
			int nbPersonnes) {
		super(dateDebut, dateFin, sejour, voyageur, nbPersonnes);
		this.validee = false;
        this.annulee = false;
	}

    public boolean isValidee() {
        return validee;
    }

    public void setValidee(boolean validee) {
        this.validee = validee;
    }

    public boolean isAnnulee() {
        return annulee;
    }

    public void setAnnulee(boolean annulee) {
        this.annulee = annulee;
    }

    public void valider() {
        if (!this.validee && !this.annulee) {
            this.validee = true;
//            this.sejour.addReservation(new DemandeReservation(this.getDateDebut(), this.getDateFin(), this.getSejour(), this.getVoyageur(), this.getNbPersonnes()));
        }
    }

    public void annuler() {
        if (!this.validee && !this.annulee) {
            this.annulee = true;
        }
    }
}
