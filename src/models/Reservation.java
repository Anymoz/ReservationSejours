package models;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Reservation {
    private LocalDate dateDebut;
    private LocalDate dateFin;
    protected Sejour sejour;
    private Voyageur voyageur;
    private String statut;
    private int nbPersonnes;

    public Reservation(LocalDate dateDebut, LocalDate dateFin, Sejour sejour, Voyageur voyageur, int nbPersonnes) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.sejour = sejour;
        this.voyageur = voyageur;
        this.statut = "en attente";
        this.nbPersonnes = nbPersonnes;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public Sejour getSejour() {
        return sejour;
    }

    public Voyageur getVoyageur() {
        return voyageur;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public double getPrixTotal() {
        return sejour.getPrix() * ChronoUnit.DAYS.between(dateDebut, dateFin);
    }

	public int getNbPersonnes() {
		return nbPersonnes;
	}

	public void setNbPersonnes(int nbPersonnes) {
		this.nbPersonnes = nbPersonnes;
	}
}
