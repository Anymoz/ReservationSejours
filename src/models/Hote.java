package models;

import java.util.ArrayList;
import java.util.List;

public class Hote extends Utilisateur {
    private String adresse;
    private List<Sejour> sejours;
    private List<DemandeReservation> demandesEnCours;

    public Hote(String nom, String prenom, String email, String motDePasse, String adresse) {
        super(nom, prenom, email, motDePasse, "hote");
        this.adresse = adresse;
        this.sejours = new ArrayList<>();
        this.demandesEnCours = new ArrayList<>();
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public List<Sejour> getSejours() {
        return sejours;
    }

    public void setSejours(List<Sejour> sejours) {
        this.sejours = sejours;
    }

    public List<DemandeReservation> getDemandesEnCours() {
        return demandesEnCours;
    }

    public void setDemandesEnCours(DemandeReservation demandesEnCours) {
        this.demandesEnCours.add(demandesEnCours);
    }

    public void ajouterSejour(Sejour sejour) {
        sejours.add(sejour);
    }

    public void supprimerSejour(Sejour sejour) {
        sejours.remove(sejour);
    }

    public void ajouterDemande(DemandeReservation demande) {
        demandesEnCours.add(demande);
    }

    public void validerDemande(DemandeReservation demande) {
        demande.valider();
    }

    public void annulerDemande(DemandeReservation demande) {
        demande.annuler();
    }

    public List<Reservation> getReservationsValidees() {
        List<Reservation> reservationsValidees = new ArrayList<>();
        for (Sejour sejour : sejours) {
            reservationsValidees.addAll(sejour.getReservations());
        }
        return reservationsValidees;
    }
}
