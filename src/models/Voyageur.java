package models;

import java.util.ArrayList;
import java.util.List;

public class Voyageur extends Utilisateur {
    private List<Reservation> reservationsEnCours;

    public Voyageur(String nom, String prenom, String email, String motDePasse) {
        super(nom, prenom, email, motDePasse, "voyageur");
        this.reservationsEnCours = new ArrayList<>();
    }

    public void ajouterReservation(Reservation reservation) {
    	reservationsEnCours.add(reservation);
    }

    public void supprimerReservation(Reservation reservation) {
    	reservationsEnCours.remove(reservation);
    }

	public List<Reservation> getReservationsEnCours() {
		return reservationsEnCours;
	}
}
