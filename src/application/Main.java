package application;
	
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import models.DemandeReservation;
import models.Hote;
import models.Panier;
import models.Reservation;
import models.Sejour;
import models.Utilisateur;
import models.Voyageur;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
		
		List<Sejour> listeSejours= new ArrayList<>();
		Hote hote = new Hote("Maazouz", "Zouhair", "zouhirmaazouz@email.com", "1234", "Lyon");
		Utilisateur user = new Utilisateur("Random", "Random", "", "", "non_auth");
		Voyageur voyageur = new Voyageur("Mistral", "Olivier", "molivier@email.com", "123456");
		
	    LocalDate dateDebut = LocalDate.now();
	    LocalDate dateFin = dateDebut.plusDays(7);;

	    
		Sejour sejour1 = new Sejour(dateDebut, dateFin, "Grenoble", 200.0, "Chambre 24m", 2 ,  hote);
		Sejour sejour2 = new Sejour(dateDebut, dateFin, "Lyon", 400.0, "Appartement de 50m", 3 ,  hote);
		Sejour sejour3 = new Sejour(dateDebut, dateFin, "Marseille", 400.0, "Maison", 3 ,  hote);
		
		hote.ajouterSejour(sejour1);
		hote.ajouterSejour(sejour2);
		hote.ajouterSejour(sejour3);
		
		
		Panier panier = new Panier(voyageur, listeSejours);
		panier.ajouterSejour(sejour3);
		panier.ajouterSejour(sejour1);
		
		panier.validerPanier();
		
		System.out.println("---------- Liste de reservations du voyageurs --------");
		for (Reservation reservation : voyageur.getReservationsEnCours()) {
			System.out.println(reservation.getSejour().getTitre());
		}
		
		System.out.println("---------- Liste de reservations chez le hote -------");
		for (Reservation reservation : hote.getDemandesEnCours()) {
			System.out.println(reservation.getSejour().getTitre());
		}
		
		System.out.println("---------- Liste de sejoures proposés par le hote -------");
		for (Sejour sejour : hote.getSejours()) {
			System.out.println(sejour.getTitre());
		}
		
		System.out.println("---------- Valider une reservation -------");
//		hote.getSejours().get(0).getReservations().get(0).valider();;
		for (Sejour sejour : hote.getSejours()) {
			for (DemandeReservation reservation : sejour.getReservations()) {
				System.out.println(reservation.getSejour().getTitre()+ " " + reservation.isValidee());
			}
			
		}
	}
}
